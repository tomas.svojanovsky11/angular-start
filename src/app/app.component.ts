import { Component } from '@angular/core'

import { User } from './users/services/users.service'

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  name = ''

  users: User[] = [
    {
      id: 'dfdfsd',
      name: 'Pavel',
      surname: 'Zeleny',
      createdAt: new Date().toISOString(),
    },
    {
      id: 'gfgfg',
      name: 'Jana ',
      surname: 'Fialova',
      createdAt: new Date().toISOString(),
    },
  ]

  trackByFn (_: number, user: User) {
    return user.id
  }

  onClick (name: string) {
    this.name = name
  }
}
