import { Injectable } from '@angular/core'

import { of } from 'rxjs'

export type User = {
  id: string
  name: string
  surname: string
  createdAt: string
}

@Injectable({
  providedIn: 'root'
})
export class UsersService {

  // constructor (private http: HttpClient) {
  // }

  getUserList () {
    return of([
      {
        id: 'dfdfsd',
        name: 'Pavel',
        surname: 'Zeleny',
        createdAt: new Date().toISOString()
      },
      {
        id: 'gfgfg',
        name: 'Jana ',
        surname: 'Fialova',
        createdAt: new Date().toISOString()
      },
    ])
  }

  // getUserList () {
  //   return this.http.get(`${environment.url}/user-list`)
  // }
}
