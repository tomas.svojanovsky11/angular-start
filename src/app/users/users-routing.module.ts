import { NgModule } from '@angular/core'
import { RouterModule, Routes } from '@angular/router'

import { UserListComponent } from './pages/user-list/user-list.component'
import { AuthorizedLayoutComponent } from '../shared/authorized-layout/authorized-layout.component'

const routes: Routes = [
  {
    path: '',
    component: AuthorizedLayoutComponent,
    children: [
      {
        path: '',
        component: UserListComponent,
      },
    ],
  },
]

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UsersRoutingModule {
}
