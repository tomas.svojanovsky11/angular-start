import { Component } from '@angular/core'

import { User, UsersService } from '../../services/users.service'

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.scss']
})
export class UserListComponent {

  users$ = this.userListService.getUserList()

  constructor (private userListService: UsersService) {
  }

  trackByFn (_: number, user: User) {
    return user.id
  }

}
