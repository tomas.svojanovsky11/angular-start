import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'

import { UserListComponent } from './pages/user-list/user-list.component'
import { UsersRoutingModule } from './users-routing.module'
import { SharedModule } from '../shared/shared.module'
import { UsersService } from './services/users.service'

@NgModule({
  declarations: [
    UserListComponent
  ],
  imports: [
    CommonModule,
    UsersRoutingModule,
    SharedModule,
  ],
  providers: [
    UsersService,
  ],
  exports: [],
})
export class UsersModule {
}
