import { ChangeDetectionStrategy, Component } from '@angular/core'

@Component({
  selector: 'app-authorized-layout',
  templateUrl: './authorized-layout.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AuthorizedLayoutComponent {

}
