import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core'

@Component({
  selector: 'app-button',
  templateUrl: './button.component.html',
  styleUrls: ['./button.component.scss']
})
export class ButtonComponent implements OnInit {

  @Input() label = ''

  @Output() outClick = new EventEmitter<string>()

  constructor () {
  }

  ngOnInit (): void {
  }

  onClick () {
    this.outClick.emit('Jak se mas')
  }

}
