import { NgModule } from '@angular/core'
import { RouterModule } from '@angular/router'

import { ButtonComponent } from './button/button.component'
import { AuthorizedLayoutComponent } from './authorized-layout/authorized-layout.component'

@NgModule({
  declarations: [
    ButtonComponent,
    AuthorizedLayoutComponent,
  ],
  exports: [
    ButtonComponent,
    AuthorizedLayoutComponent,
  ],
  imports: [
    RouterModule,
  ]
})
export class SharedModule {
}
