import { NgModule } from '@angular/core'
import { RouterModule, Routes } from '@angular/router'

import { InvoiceListComponent } from './pages/invoice-list/invoice-list.component'
import { AuthorizedLayoutComponent } from '../shared/authorized-layout/authorized-layout.component'

const routes: Routes = [
  {
    path: '',
    component: AuthorizedLayoutComponent,
    children: [
      {
        path: '',
        component: InvoiceListComponent,
      },
    ],
  },
]

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class InvoicesRoutingModule {
}
