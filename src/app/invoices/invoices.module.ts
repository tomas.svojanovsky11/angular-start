import { NgModule } from '@angular/core'

import { InvoiceListComponent } from './pages/invoice-list/invoice-list.component'
import { InvoicesRoutingModule } from './invoices-routing.module'
import { SharedModule } from '../shared/shared.module'

@NgModule({
  declarations: [
    InvoiceListComponent
  ],
  imports: [
    InvoicesRoutingModule,
    SharedModule,
  ],
})
export class InvoicesModule {

}
